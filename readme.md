Splitwise

Requirements :

1)Create user  
2)Create groups  
3)Add people to group  
4)Create expense  
&nbsp;&nbsp;&nbsp;&nbsp;4.1) ExactSplit  
&nbsp;&nbsp;&nbsp;&nbsp;4.2) EqualSplit  
&nbsp;&nbsp;&nbsp;&nbsp;4.2) ShareSplit  
5)Show balance for user  
6)Settle Balance for user
---------------------------------------------------
Launching application :  
Requirement : JDK 11  
1) clone project
2) inside splitwise folder  
   &nbsp;&nbsp;&nbsp;&nbsp; gradlew.bat bootRun

Database URL :   
http://localhost:8080/h2-console

Open API :  
http://localhost:8080/swagger-ui/index.html
