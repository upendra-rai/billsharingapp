package dev.upendra.splitwise.controller;


import dev.upendra.splitwise.dto.BalanceDTO;
import dev.upendra.splitwise.entities.Expense;
import dev.upendra.splitwise.exception.IncorrectDataException;
import dev.upendra.splitwise.services.ExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/expense")
public class ExpenseController {
    @Autowired
    private ExpenseService expenseService;

    @PostMapping
    public ResponseEntity addExpense(@RequestBody Expense expense) throws IncorrectDataException {
        expenseService.createExpense(expense);
        return new ResponseEntity(HttpStatus.CREATED);
    }
    @GetMapping(path="/group/{groupId}/user/{userId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<BalanceDTO>> getStatement(@PathVariable Long groupId, @PathVariable Long userId){
        List<BalanceDTO> balance = expenseService.getBalanceForUserWithinGroup(groupId,userId);
        ResponseEntity<List<BalanceDTO>> response;
        if(balance.size()!=0)
            response =new ResponseEntity<>(expenseService.getBalanceForUserWithinGroup(groupId,userId),HttpStatus.OK);
        else
            response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return response;
    }

    /***
     *
     * @param groupId settlement will happen in between user of this group
     * @param userId   user 1 involved in settlement
     * @param userId2 user2 involved in settlement
     * @return
     * @throws IncorrectDataException
     */
    @PutMapping(path="/settle/group/{groupId}/user/{userId}/with/{userId2}")
    public ResponseEntity<List<BalanceDTO>> settleUp(@PathVariable Long groupId,@PathVariable Long userId,
                                                     @PathVariable Long userId2) throws IncorrectDataException {
        expenseService.settleBalance(groupId, userId, userId2);
        return new ResponseEntity<>(HttpStatus.OK);
    }



}