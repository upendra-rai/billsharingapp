package dev.upendra.splitwise.controller;

import dev.upendra.splitwise.entities.ExpenseGroup;
import dev.upendra.splitwise.services.ExpenseGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/groups")
public class GroupController {
    @Autowired
    ExpenseGroupService expenseGroupService;

    @PostMapping
    public ResponseEntity<ExpenseGroup> createExpenseGroup(@RequestBody ExpenseGroup expenseGroup){
        expenseGroupService.createExpenseGroup(expenseGroup);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

//    @PutMapping
//    public ResponseEntity<ExpenseGroup> addUserToGroup(@RequestBody ExpenseGroup expenseGroup){
//        expenseGroupService.
//    }

    @GetMapping
    public ResponseEntity<List<ExpenseGroup>> listGroups(){
        List<ExpenseGroup> expenseGroups = expenseGroupService.findAll();
        return new ResponseEntity<>(expenseGroups,HttpStatus.OK);
    }

}