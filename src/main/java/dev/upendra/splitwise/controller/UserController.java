package dev.upendra.splitwise.controller;

import dev.upendra.splitwise.entities.User;
import dev.upendra.splitwise.services.UserService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private  static Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<User> createUser(@Valid @RequestBody User user){
        logger.info("Creating new user"+user);
        userService.createUser(user);
        logger.info("New User created successfully, user  {}",user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<User>> listUsers(){
        List<User> users = userService.findAll();
        return new ResponseEntity<>(users,HttpStatus.OK);
    }



}