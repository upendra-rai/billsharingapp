package dev.upendra.splitwise.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="EXPENSE")
@Data
@NoArgsConstructor
public class Expense {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="EXPENSE_ID")
    private Long expenseId;
    @Column(name="EXPENSE_LABEL")
    private String expenseLabel;
    @OneToOne
    @JoinColumn(name = "EXPENSE_GROUP", referencedColumnName = "GROUP_ID")
    private ExpenseGroup expenseGroup;
    private double amount;
    @OneToMany(mappedBy = "expense",cascade= CascadeType.ALL)
    private List<ExpenseDetail> details;
    private ExpenseType expenseType;



}