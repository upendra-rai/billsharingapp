package dev.upendra.splitwise.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name="EXPENSE_DETAIL")
@Data
@NoArgsConstructor
@ToString
public class ExpenseDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="EXPENSE_DETAIL_ID")
    private Long expenseDetailId;
    @ManyToOne
    @JoinColumn(name="EXPENSE_ID")
    private Expense expense;
    @ManyToOne
    @JoinColumn(name="USER_ID")
    private User user;
    @Column(name="CONTRIBUTION_SHARE")
    private double userContributionShare;
    @Column(name="EXPEND_SHARE")
    private double userExpendShare;

}