package dev.upendra.splitwise.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="EXPENSE_GROUP")
@Data
@NoArgsConstructor
@ToString
public class ExpenseGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="GROUP_ID")
    private Long groupId;
    @Column(name="GROUP_NAME")
    private String groupName;
    @ManyToMany
    @JoinTable(name = "USER_GROUP", joinColumns = { @JoinColumn(name = "GROUP_ID") }, inverseJoinColumns = { @JoinColumn(name = "USER_ID") })
    private Set<User> members;

}