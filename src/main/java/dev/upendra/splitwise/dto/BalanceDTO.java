package dev.upendra.splitwise.dto;

import dev.upendra.splitwise.entities.User;
import lombok.Data;

@Data
public class BalanceDTO {

    private User giver;  //
    private User receiver;
    private double amount;

}
