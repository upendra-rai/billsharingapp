package dev.upendra.splitwise.services;

import dev.upendra.splitwise.dto.BalanceDTO;
import dev.upendra.splitwise.entities.*;
import dev.upendra.splitwise.exception.IncorrectDataException;
import dev.upendra.splitwise.repository.ExpenseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExpenseService {
    Logger logger = LoggerFactory.getLogger(ExpenseService.class);
    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private ExpenseGroupService expenseGroupService;

    @Autowired
    private UserService userService;

    /***
     *This method will create Expense based on the type of expense sharing chosen
     * and the data provided for each user involved in expense.
     * @param expense  expense details
     * @throws IncorrectDataException will through exception if the expense data is inconsistent.
     */
    public void createExpense(Expense expense) throws IncorrectDataException {
        logger.info("Started saving expense "+expense);
        //splitExpense(expense);
        ExpenseSplittingStrategy strategy = ExpenseSplittingStrategyFactory.getExpenseSplittingStrategy(expense);
        strategy.validateAndSplit();
        expense.getDetails().forEach(expenseDetail -> expenseDetail.setExpense(expense));
        try {
            expenseRepository.save(expense);
        }catch (Exception ex){
            logger.error("Error saving expense "+expense, ex);
        }
    }

    /***
     * This method will generate a statement(credit/debt w.r.t other user) for user within given group
     * @param groupId  group for which statement will be created
     * @param userId  user within the group for which statement will be generated
     * @return list of balances for this user in this group
     */
    public List<BalanceDTO> getBalanceForUserWithinGroup(Long groupId,Long userId){
        ExpenseGroup group = expenseGroupService.findById(groupId);
        Map<Long,Double> giver =  new LinkedHashMap<>();
        Map<Long,Double> receiver =  new LinkedHashMap<>();
        List<BalanceDTO> balances = new ArrayList<>();
        for(User user : group.getMembers()){
            Double contributionByUserInGroup = expenseRepository.getContributionByUserInGroup(group, user);
            Double expendByUserInGroup = expenseRepository.getExpendByUserInGroup(group, user);
            if(contributionByUserInGroup==null ||expendByUserInGroup==null)
                return balances;
            double diff = contributionByUserInGroup - expendByUserInGroup;
            if(diff==0)
                new ArrayList<BalanceDTO>();
            if(diff < 0 )
                giver.put(user.getUserId(),-1*diff);
            else
                receiver.put(user.getUserId(),diff);
        }
        giver = sortByValue(giver,false);   // giver sorted in desc  (giver values are absolute)
        receiver = sortByValue(receiver,false);   //receiver sorted in desc order

        Iterator<Long> giverItr = giver.keySet().iterator();
        /***
         * start settling the highest giver against the highest receiver
         */
        while(giverItr.hasNext()){
            BalanceDTO balance ;
            long giverKey = giverItr.next();
            double giverAmount = giver.get(giverKey);

            Iterator<Long> receiverItr = receiver.keySet().iterator();
            while(receiverItr.hasNext()) {
                balance= new BalanceDTO();
                long receiverKey = receiverItr.next();

                double receiverAmount = receiver.get(receiverKey);
                if(receiverAmount==0.0 )
                    continue;
                if (giverAmount > receiverAmount) {
                    giverAmount = giverAmount - receiverAmount;
                    receiverItr.remove();
                    giver.put(giverKey, giverAmount);
                    balance.setAmount(receiverAmount);
                } else if (giverAmount < receiverAmount) {
                    receiverAmount = receiverAmount - giverAmount;

                    receiver.put(receiverKey, receiverAmount);
                    balance.setAmount(giverAmount);
                    giverAmount=0;
                } else {
                    balance.setAmount(receiverAmount);
                    receiverItr.remove();
                    giverAmount=0;

                }
                balance.setGiver(userService.findById(giverKey));
                balance.setReceiver(userService.findById(receiverKey));
                if(giverKey==userId||receiverKey==userId)
                    balances.add(balance);
                if(giverAmount==0)
                    break;
            }
        }

        return balances;
    }

    /***
     * This method will settle the balance between given 2 users in a group. A settlement request from any user
     * will create a new counter expense(settlement).
     * ex
     *      if A owes B - 20INR
     * Any user A or B can initiate to settle , this will create new expense of 20 INR
     * where A will be contributor and only B will be involved(consumer) for that expense.
     *
     * @param groupId   group within which settlement initiated
     * @param user1     user 1 involved in settlement
     * @param user2     user 2 involved in settlement
     * @throws IncorrectDataException
     */

    public void settleBalance(Long groupId,Long user1, Long user2) throws IncorrectDataException {
        List<BalanceDTO> balance = getBalanceForUserWithinGroup(groupId,user1);
        Expense expense = new Expense();
        expense.setExpenseLabel("Settlement");

        ExpenseGroup group =new ExpenseGroup();group.setGroupId(groupId);
        expense.setExpenseGroup(group);
        expense.setExpenseType(ExpenseType.EXACT);
        List<ExpenseDetail> expenseDetails = new ArrayList<>();
        ExpenseDetail detail1 =new ExpenseDetail();
        ExpenseDetail detail2 =new ExpenseDetail();
        expenseDetails.add(detail1);
        expenseDetails.add(detail2);
        expense.setDetails(expenseDetails);
        for(BalanceDTO dto : balance){
            if(dto.getGiver().getUserId()==user1 &&dto.getReceiver().getUserId()==user2)
            {
                expense.setAmount(dto.getAmount());
                detail1.setUser(userService.findById(user1));
                detail1.setUserContributionShare(dto.getAmount());
                detail2.setUser(userService.findById(user2));
                detail2.setUserExpendShare(dto.getAmount());
                break;

            }else if(dto.getReceiver().getUserId()==user1 && dto.getGiver().getUserId()==user2){
                expense.setAmount(dto.getAmount());
                detail1.setUser(userService.findById(user2));
                detail1.setUserContributionShare(dto.getAmount());
                detail2.setUser(userService.findById(user1));
                detail2.setUserExpendShare(dto.getAmount());
                break;
            }
        }
        createExpense(expense);

    }

    /***
     * This is a utility method to sort map by value. As it is used in only this service hence kept here
     * @param unsortMap  input Map
     * @param order true - ASC, false-DESC
     * @return  returns a map sorted by value.
     */
    private static Map<Long, Double> sortByValue(Map<Long,Double> unsortMap, final boolean order)
    {
        List<Map.Entry<Long, Double>> list = new LinkedList<>(unsortMap.entrySet());


        list.sort((o1, o2) -> order ? o1.getValue().compareTo(o2.getValue()) == 0
                ? o1.getKey().compareTo(o2.getKey())
                : o1.getValue().compareTo(o2.getValue()) : o2.getValue().compareTo(o1.getValue()) == 0
                ? o2.getKey().compareTo(o1.getKey())
                : o2.getValue().compareTo(o1.getValue()));
        return list.stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> b, LinkedHashMap::new));


    }


}
