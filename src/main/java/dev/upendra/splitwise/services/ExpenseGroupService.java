package dev.upendra.splitwise.services;

import dev.upendra.splitwise.entities.ExpenseGroup;
import dev.upendra.splitwise.entities.User;
import dev.upendra.splitwise.exception.ResourceNotFoundException;
import dev.upendra.splitwise.repository.ExpenseGroupRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ExpenseGroupService {
    Logger logger = LoggerFactory.getLogger(ExpenseGroupService.class);
    @Autowired
    private ExpenseGroupRepository repository;

    @Autowired
    private UserService userService;

    public void createExpenseGroup(ExpenseGroup group) throws ResourceNotFoundException {
        group.getMembers().forEach(x->userService.findById(x.getUserId()));
        repository.save(group);
    }

    public List<ExpenseGroup> findAll(){
        return repository.findAll();
    }

    public ExpenseGroup findById(Long id){

        Optional<ExpenseGroup> groupOptional = repository.findById(id);
        ExpenseGroup expenseGroup=null;
        try {
            expenseGroup= groupOptional.get();
        }catch (NoSuchElementException ex){
            logger.error("Group Not found, group id:"+id,ex);
            throw new  ResourceNotFoundException("Group Not found, group id :"+ id);
        }catch (Exception ex){
            logger.error("Error occurred while fetching user"+ ex);
        }
        return expenseGroup;
    }

    public void addUserToGroup(Long groupId, User user){
        ExpenseGroup group = repository.findById(groupId).orElseThrow(()->new ResourceNotFoundException("Group Not found "+groupId));
        group.getMembers().add(user);
    }


}
