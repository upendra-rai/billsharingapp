package dev.upendra.splitwise.services;

import dev.upendra.splitwise.entities.Expense;
import dev.upendra.splitwise.entities.ExpenseDetail;
import dev.upendra.splitwise.exception.IncorrectDataException;

public class PercentSplittingStrategy extends ExpenseSplittingStrategy{
    public PercentSplittingStrategy(Expense expense) {
        super(expense);
    }

    @Override
    Expense split() throws IncorrectDataException {
        double totalPercent=0;
        double amount =expense.getAmount();
        for(ExpenseDetail details: expense.getDetails()){
            totalPercent+=details.getUserExpendShare();
            details.setUserExpendShare(amount*details.getUserExpendShare()/100.0);
        }
        if(totalPercent!=100)
            throw new IncorrectDataException("Total Expend percentage cannot be more/less" +
                    " than 100, current percentage :"+totalPercent);
        return expense;
    }
}
