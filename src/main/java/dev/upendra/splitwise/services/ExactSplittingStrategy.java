package dev.upendra.splitwise.services;

import dev.upendra.splitwise.entities.Expense;
import dev.upendra.splitwise.entities.ExpenseDetail;
import dev.upendra.splitwise.exception.IncorrectDataException;

public class ExactSplittingStrategy extends ExpenseSplittingStrategy{
    public ExactSplittingStrategy(Expense expense) {
        super(expense);
    }

    @Override
    public Expense split() throws IncorrectDataException {
        double totalExpend=0.0;

        for(ExpenseDetail details: expense.getDetails()){
            totalExpend+=details.getUserExpendShare();
        }

        if(totalExpend!=expense.getAmount())
            throw new IncorrectDataException("Expense amount should be equal to sum of expend amount");
        return expense;
    }
}
