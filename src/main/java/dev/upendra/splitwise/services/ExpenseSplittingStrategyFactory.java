package dev.upendra.splitwise.services;

import dev.upendra.splitwise.entities.Expense;
import dev.upendra.splitwise.exception.IncorrectDataException;

public class ExpenseSplittingStrategyFactory {

    public static ExpenseSplittingStrategy getExpenseSplittingStrategy(Expense expense) throws IncorrectDataException {
        switch (expense.getExpenseType()){
            case EQUAL:
                    return new EqualSplittingStrategy(expense);
            case EXACT:
                    return new ExactSplittingStrategy(expense);
            case PERCENTAGE:
                    return new PercentSplittingStrategy(expense);
            default:
               throw new IncorrectDataException("Not Valid a valid expense");
        }
    }

}
