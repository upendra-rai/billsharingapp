package dev.upendra.splitwise.services;

import dev.upendra.splitwise.entities.Expense;
import dev.upendra.splitwise.exception.IncorrectDataException;

public class EqualSplittingStrategy extends ExpenseSplittingStrategy{

    public EqualSplittingStrategy(Expense expense) {
        super(expense);
    }

    @Override
    Expense split() throws IncorrectDataException {
        int totalSplits = expense.getDetails().size();
        double amount =expense.getAmount();
        double splitAmount = ((double) Math.round(amount*100/totalSplits))/100.0;
        expense.getDetails().forEach(x->x.setUserExpendShare(splitAmount));
        expense.getDetails().get(0).setUserExpendShare(splitAmount+(amount-splitAmount*totalSplits));
        return expense;
    }
}
