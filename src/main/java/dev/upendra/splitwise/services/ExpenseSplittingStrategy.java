package dev.upendra.splitwise.services;

import dev.upendra.splitwise.entities.Expense;
import dev.upendra.splitwise.entities.ExpenseDetail;
import dev.upendra.splitwise.exception.IncorrectDataException;
import lombok.Data;

@Data
public abstract class ExpenseSplittingStrategy {
    protected Expense expense;

    public ExpenseSplittingStrategy(Expense expense) {
        this.expense = expense;
    }

    public Expense validateAndSplit() throws IncorrectDataException {
        double amount = expense.getAmount();
        double totalContribution= 0.0;

        for(ExpenseDetail details: expense.getDetails()){
            totalContribution+= details.getUserContributionShare();
        }
        if(totalContribution!=amount)
            throw new IncorrectDataException("Expense amount should be equal to sum of contribution amount");
        split();
        return null;

    }
    abstract Expense split() throws IncorrectDataException;

}
