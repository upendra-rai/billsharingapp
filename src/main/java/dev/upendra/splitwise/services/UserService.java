package dev.upendra.splitwise.services;

import dev.upendra.splitwise.entities.User;
import dev.upendra.splitwise.exception.ResourceNotFoundException;
import dev.upendra.splitwise.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class UserService {
    Logger logger = LoggerFactory.getLogger(UserService.class);
    @Autowired
    private UserRepository userRepository;

    public void createUser(User user){
        try {
            userRepository.save(user);
        }catch (Exception ex){
            logger.error("Error occurred saving user :"+user);
        }
    }
    public List<User> findAll(){
        return userRepository.findAll();
    }
    public User findById(Long userId) throws ResourceNotFoundException {
        Optional<User> userOptional = userRepository.findById(userId);
        User user=null;
        if(userOptional.isPresent())
            user= userOptional.get();
        else {
            logger.error("User not found, user id :" + userId);
            throw new ResourceNotFoundException("User not found :" + userId);
        }
        return user;
    }
}

