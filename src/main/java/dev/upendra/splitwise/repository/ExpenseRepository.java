package dev.upendra.splitwise.repository;

import dev.upendra.splitwise.entities.Expense;
import dev.upendra.splitwise.entities.ExpenseGroup;
import dev.upendra.splitwise.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExpenseRepository extends JpaRepository<Expense,Long> {

    List<Expense> findByExpenseGroup(ExpenseGroup groupId);

    @Query(
            value = "SELECT sum(d.contribution_share) FROM expense e left join expense_detail d on e.expense_id=d.expense_id" +
                    " where e.expense_group=:groupId  and d.user_id=:userId group by d.user_id",
            nativeQuery = true)
    Double getContributionByUserInGroup(@Param("groupId") ExpenseGroup group, @Param("userId") User user);
    @Query(
            value = "SELECT sum(d.expend_share) FROM expense e left join expense_detail d on e.expense_id=d.expense_id" +
                    " where e.expense_group=:groupId  and d.user_id=:userId group by d.user_id",
            nativeQuery = true)
    Double getExpendByUserInGroup(@Param("groupId") ExpenseGroup group, @Param("userId") User user);
}