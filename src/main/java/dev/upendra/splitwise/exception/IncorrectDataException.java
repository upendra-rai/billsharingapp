package dev.upendra.splitwise.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


public class IncorrectDataException extends Exception{
    public IncorrectDataException(String message){
        super(message);
    }

}
